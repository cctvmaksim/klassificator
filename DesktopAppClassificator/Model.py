import tensorflow as tf
import numpy as np
from tensorflow import keras
import math


def predicton(Data):
    reconstructed_model = keras.models.load_model("my_model")
    prediction = reconstructed_model.predict(Data)
    return prediction
